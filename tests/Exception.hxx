#ifndef TESTS_EXCEPTION_HXX
#define TESTS_EXCEPTION_HXX

#include <cppunit/extensions/HelperMacros.h>

namespace tests
{
	class Exception;
}

class tests::Exception : public CppUnit::TestFixture
{
CPPUNIT_TEST_SUITE(Exception);
	CPPUNIT_TEST(testCopyingOfShortMessage);
	CPPUNIT_TEST(testWhetherLongMessageDoesNotOverflow);
	CPPUNIT_TEST(testWhetherPreviousExceptionIsCloned);
	CPPUNIT_TEST(testWhetherClonedExceptionIsDeleted);
	CPPUNIT_TEST(testWhetherBadAllocOnCloneIsSwallowed);
	CPPUNIT_TEST(testWhetherNoticeIsAppendedOnBadAlloc);
	CPPUNIT_TEST(testWhetherNoticeDoesNotOverflowIfBufferTooSmall);
	CPPUNIT_TEST(testWritingToStream);
	CPPUNIT_TEST(testBacktrace);
CPPUNIT_TEST_SUITE_END();

public:
	void testCopyingOfShortMessage();
	void testWhetherLongMessageDoesNotOverflow();
	void testWhetherPreviousExceptionIsCloned();
	void testWhetherClonedExceptionIsDeleted();
	void testWhetherBadAllocOnCloneIsSwallowed();
	void testWhetherNoticeIsAppendedOnBadAlloc();
	void testWhetherNoticeDoesNotOverflowIfBufferTooSmall();
	void testWritingToStream();
	void testBacktrace();
};

#endif
