#include <tests/Utility.hxx>
#include <tests/framework/MockException.hxx>

#include <libsex/utility.hxx>

#include <sstream>

void tests::Utility::testShortTemplateWithoutFormatting()
{
	char buf[100] = { 0 };

	CPPUNIT_ASSERT(
		libsex::format(
			buf, 100,
			"foobar.cxx", 42,
			"Message template."));

	std::string exp("foobar.cxx:42: Message template.");
	CPPUNIT_ASSERT_EQUAL(exp, std::string(buf));
}

void tests::Utility::testWhetherFalseIsReturnedWhenHeaderFillsBuffer()
{
	char buf[100] = { 0 };

	CPPUNIT_ASSERT(
		!libsex::format(
			buf, 16,
			"foobar.cxx", 42,
			"Message template."));

	std::string exp("foobar.cxx:42: ");
	CPPUNIT_ASSERT_EQUAL(exp, std::string(buf));
}

void tests::Utility::testWhetherTrueIsReturnedOnExactFit()
{
	char buf[100] = { 0 };

	CPPUNIT_ASSERT(
		libsex::format(
			buf, 33,
			"foobar.cxx", 42,
			"Message template."));

	std::string exp("foobar.cxx:42: Message template.");
	CPPUNIT_ASSERT_EQUAL(exp, std::string(buf));
}

void tests::Utility::testWhetherUnformattedOverflowsArePrevented()
{
	// Array should be large enough to not really get
	// overwritten. Just adjust the parameter 'length'
	// to libsex::format().
	char buf[100] = { 0 };

	CPPUNIT_ASSERT(
		!libsex::format(
			buf, 10,
			"foobar.cxx", 42,
			"Message template."));

	assertBoundary(buf, 10, 100);
}

void tests::Utility::testWhetherSimpleFormattingWorks()
{
	char buf[100] = { 0 };

	CPPUNIT_ASSERT(
		libsex::format(
			buf, 100,
			"foobar.cxx", 42,
			"String: '%s'.", "STRING"));

	std::string exp("foobar.cxx:42: String: 'STRING'.");
	CPPUNIT_ASSERT_EQUAL(exp, std::string(buf));
}

void tests::Utility::testWhetherFormattedOverflowsArePreventedBeforeFormatstring()
{
	// Array should be large enough to not really get
	// overwritten. Just adjust the parameter 'length'
	// to libsex::format().
	char buf[100] = { 0 };

	CPPUNIT_ASSERT(
		!libsex::format(
			buf, 10,
			"foobar.cxx", 42,
			"String: '%s'.", "STRING"));

	assertBoundary(buf, 10, 100);
}

void tests::Utility::testWhetherFormattedOverflowsArePreventedInFormatstring()
{
	// Array should be large enough to not really get
	// overwritten. Just adjust the parameter 'length'
	// to libsex::format().
	char buf[100] = { 0 };

	CPPUNIT_ASSERT(
		!libsex::format(
			buf, 28,
			"foobar.cxx", 42,
			"String: '%s'.", "STRING"));

	assertBoundary(buf, 28, 100);
}

void tests::Utility::testMultipleFormatParameters()
{
	char buf[100] = { 0 };

	CPPUNIT_ASSERT(
		libsex::format(
			buf, 100,
			"foobar.cxx", 42,
			"%s=%d", "var", 42));

	std::string exp("foobar.cxx:42: var=42");
	CPPUNIT_ASSERT_EQUAL(exp, std::string(buf));
}

void tests::Utility::testWhetherMultipleParametersDontOverflowBeforeFormatstring()
{
	char buf[100] = { 0 };

	CPPUNIT_ASSERT(
		!libsex::format(
			buf, 10,
			"foobar.cxx", 42,
			"%s=%d", "var", 42));

	assertBoundary(buf, 10, 100);
}

void tests::Utility::testWhetherMultipleParametersDontOverflowInFormatstring()
{
	char buf[100] = { 0 };

	CPPUNIT_ASSERT(
		!libsex::format(
			buf, 18,
			"foobar.cxx", 42,
			"%s=%d", "var", 42));

	assertBoundary(buf, 18, 100);
}

void tests::Utility::testTemplateFactory()
{
	MockException e
		= libsex::formatted<MockException>(
			"foobar.cxx", 42,
			"VAR", 42);

	CPPUNIT_ASSERT_EQUAL(
		std::string("foobar.cxx:42: VAR=42"),
		std::string(e.what()));
}

void tests::Utility::testChainedTemplateFactory()
{
	MockException e1
		= libsex::formatted<MockException>(
			"foobar.cxx", 42,
			"VAR", 42);

	MockException e = libsex::formatted<MockException>(
		e1,
		"foobar.cxx", 84,
		"#exceptions", 1);

	std::stringstream act;
	e.backtrace(act);

	CPPUNIT_ASSERT_EQUAL(
		std::string("foobar.cxx:84: #exceptions=1\n"
		            "foobar.cxx:42: VAR=42"),
		act.str());
}

void tests::Utility::assertBoundary(
	const char* const buffer,
	size_t boundary,
	size_t length)
{
	for (size_t i = boundary; i < length; ++i) {
		CPPUNIT_ASSERT_EQUAL('\0', buffer[i]);
	}
}
