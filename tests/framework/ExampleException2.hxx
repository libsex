/**
 * @file
 *
 * Shows how to _declare_ an exception class that uses
 * placedholders in its message template.
 *
 * Note that it is _declared_ in exactly the same way as a
 * basic exception class.
 */

#ifndef EXAMPLEEXCEPTION2_HXX
#define EXAMPLEEXCEPTION2_HXX

#include <libsex/declare.hxx>
#include <libsex/Exception.hxx>

namespace example
{
	class ExampleException2;
}

// ExampleException2 inherits from libsex::Exception.
LIBSEX_DECLARE(libsex::Exception, example, ExampleException2)

#endif
