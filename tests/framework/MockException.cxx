#include <tests/framework/MockException.hxx>

const char* const MockException::TEMPLATE = "%s=%d";

MockException::MockException(const char* const message) throw()
: libsex::Exception(message)
{
}

MockException::MockException(
	const char* const message,
	const libsex::Exception& previous) throw()
: libsex::Exception(message, previous)
{
}
