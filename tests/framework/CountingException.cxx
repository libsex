#include <tests/framework/CountingException.hxx>

int CountingException::instances = 0;

CountingException::CountingException(
	const char* const message)
: libsex::Exception(message)
{
	CountingException::instances++;
}

CountingException::~CountingException() throw()
{
	CountingException::instances--;
}

const libsex::Exception*
CountingException::clone() const throw(std::bad_alloc)
{
	return new CountingException("CountingException");
}
