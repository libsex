/**
 * @file
 *
 * Definition of @ref libsex::Exception.
 */

#include <libsex/Exception.hxx>
#include <cstring> // strncpy()

libsex::Exception::Exception(const char* const message)
throw()
: _previous(0)
{
	_initMessage(message);
}

libsex::Exception::Exception(
	const char* const message,
	const Exception& previous)
throw()
: _previous(0)
{
	_initMessage(message);
	try {
		_previous = previous.clone();
	} catch (std::bad_alloc& e) {
		// We need to know the amount of characters
		// that can be put into _message.

		// Size to begin with:
		unsigned int length = Exception::LENGTH;
		// Substract characters written so far:
		length -= strlen(_message);
		// Substract nullbyte appended by strcat().
		// Prevent underflow.
		if (length > 0) --length;

		// Add notice to our original message:
		strncat(_message, "\n"
			"std::bad_alloc while cloning Exception!",
			length);
	}
}

libsex::Exception::~Exception()
throw()
{
	if (_previous) delete _previous;
}

void libsex::Exception::write(std::ostream& out) const
throw(std::ios_base::failure)
{
	out << what();
}

void libsex::Exception::backtrace(std::ostream& out) const
throw(std::ios_base::failure)
{
	write(out);
	if (_previous != 0) {
		out << "\n";
		_previous->backtrace(out);
	}
}

const libsex::Exception* libsex::Exception::clone() const
throw(std::bad_alloc)
{
	if (_previous) {
		return new Exception(_message, *_previous);
	} else {
		return new Exception(_message);
	}
}

const char* libsex::Exception::what() const
throw()
{
	return _message;
}

void libsex::Exception::_initMessage(const char* const message)
{
	strncpy(_message, message, Exception::LENGTH);
	// if |message| < |_message|
	//	strncpy() will pad with 0
	// else
	//	strncpy() will NOT add a trailing 0!
	_message[Exception::LENGTH] = 0;
}
