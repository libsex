/**
 * @file
 *
 * Macros related to throwing exceptions.
 */

#ifndef LIBSEX_THROW_HXX
#define LIBSEX_THROW_HXX

#include <libsex/utility.hxx>

/**
 * Macro to throw an exception without any argument.
 *
 * File name and line number are inserted automatically.
 *
 * @param class Which exception to throw.
 */
#define THROW_ARGLESS(class) \
{ \
	throw libsex::formatted<class>(__FILE__, __LINE__); \
}

/**
 * Macro to throw an exception with arguments.
 *
 * @param class Which exception to throw.
 * @param args... Arguments for message template.
 * @see THROW_ARGLESS()
 */
#define THROW(class, args...) \
{ \
	throw libsex::formatted<class>(__FILE__, __LINE__, ##args); \
}

/**
 * Macro to throw another exception when an exception has
 * been caught.
 *
 * Chains the previous exception and the newly created one
 * together.
 *
 * @note The previous exception has to be accessible in
 *       this scope via the variable name @c e.
 *
 * @param class   Which exception to throw.
 * @param args... Arguments for message template.
 * @see THROW
 */
#define CHAIN(class, args...) \
{\
	throw libsex::formatted<class>(e, __FILE__, __LINE__, ##args); \
}

#endif
