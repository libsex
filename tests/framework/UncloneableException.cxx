#include <tests/framework/UncloneableException.hxx>

UncloneableException::UncloneableException()
: libsex::Exception("UncloneableException") { }

UncloneableException::~UncloneableException() throw() { }

const libsex::Exception*
UncloneableException::clone() const throw(std::bad_alloc)
{
	throw std::bad_alloc();
}
