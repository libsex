/**
 * @file
 *
 * Declarations of auxiliary formatting functions intended
 * for internal use only.
 */

#ifndef LIBSEX_UTILITY_HXX
#define LIBSEX_UTILITY_HXX

#include <libsex/Exception.hxx>

#include <cstdarg> // variadic argument fun
#include <cstdio>  // size_t

namespace libsex
{
	/**
	 * Formats a message and writes it into @a buffer.
	 *
	 * It is guaranteed that not more than @a length
	 * characters are written to @a buffer.
	 *
	 * @param buffer  String to be filled.
	 * @param length  Size of buffer.
	 * @param file    Filename in which the error occured.
	 * @param line    Line number in which the error occured.
	 * @param message printf formatstring template
	 * @param ap      variadic argument list, holds parameters
	 *                to be used when formatting (vsnprintf()).
	 * @return true if buffer was large enough, false otherwise
	 */
	bool vformat(
		char* const buffer,
		size_t length,
		const char* const file,
		unsigned short line,
		const char* const message,
		va_list ap) throw();

	/// @see vformat()
	bool format(
		char* const buffer,
		size_t length,
		const char* const file,
		unsigned short line,
		const char* const message,
		...) throw();

	/**
	 * Instanciates T with properly formatted message.
	 *
	 * T is supposed to have a static const char* TEMPLATE
	 * field, which will be used as formatstring for vformat().
	 *
	 * @param file To be passed on to format().
	 * @param line To be passed on to format().
	 * @param ...  Passed to vformat() (sprintf() like).
	 * @return Instance of T.
	 */
	template <typename T>
	T formatted(
		const char* const file,
		unsigned short line,
		...) throw();

	/**
	 * Instanciates T with properly formatted message and
	 * a previous exception.
	 *
	 * @see formatted()
	 */
	template <typename T>
	T formatted(
		const Exception& previous,
		const char* const file,
		unsigned short line,
		...) throw();
}

#include "utility.ixx"

#endif
