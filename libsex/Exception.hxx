/**
 * @file
 *
 * Declaration of @ref libsex::Exception.
 */

/**
 * @mainpage Example
 *
 * The unit tests serve as compilable and runnable
 * documentation (not included in this API documentation).
 *
 * @n@n
 *
 * At first, let's declare a very primitive exception in
 * @c tests/framework/ExampleException1.hxx
 * @include framework/ExampleException1.hxx
 *
 * We then define it and assign an error message in
 * @c tests/framework/ExampleException1.cxx
 * @include framework/ExampleException1.cxx
 *
 * We repeat that procedure to create a more sophisticated
 * class:
 *
 * @c tests/framework/ExampleException2.hxx
 * @include framework/ExampleException2.hxx
 *
 * @c tests/framework/ExampleException2.cxx
 * @include framework/ExampleException2.cxx
 *
 * Finally, here's how to use those classes and the
 * framework:
 *
 * @c tests/UsageExamples.cxx
 * @include UsageExamples.cxx
 */

#ifndef LIBSEX_EXCEPTION_HXX
#define LIBSEX_EXCEPTION_HXX

#include <exception> // std::exception
#include <stdexcept> // std::bad_alloc
#include <ostream>   // std::ostream

namespace libsex
{
	class Exception;
}

/**
 * @class libsex::Exception
 *
 * Generic error made up of a message and
 * (optional) references to previous one.
 *
 * Double-fault save (not throwing std::bad_alloc
 * or other exceptions in critical situations) and
 * allows exception chaining.
 *
 * For object-oriented error handling you should
 * subclass this class for every type of error.
 * There are elegant macros in @file declare.hxx
 * and @file define.hxx.
 */
class libsex::Exception : public std::exception
{
public:
	/// Max payload of the internal character string.
	static const unsigned short LENGTH = 500;

	/**
	 * Creates an exception.
	 *
	 * Supplied character array should be null-
	 * terminated and should not exceed LENGTH
	 * characters (excl. null byte). Its contents will
	 * be copied into an internal buffer and can be
	 * overwritten/freed afterwards.
	 *
	 * The constructors do not accept std::string
	 * since that class uses the heap and may throw
	 * std::bad_alloc -- which might obviously happen
	 * even before this constructor is entered and
	 * thus might result in loss of the actual (chain
	 * of) exception(s) describing the error.
	 */
	Exception(const char* const message) throw();

	/**
	 * Creates a chained exception.
	 *
	 * Both the error message and the previous
	 * exception are cloned.
	 *
	 * Since exceptions should be created on the stack
	 * (see std::bad_alloc), the previous one will be
	 * internally copied to the heap by calling
	 * clone().
	 *
	 * If clone() throws std::bad_alloc, a notice is
	 * appended to the internal message buffer of this
	 * exception (as long as space left) and
	 * std::bad_alloc is swallowed.
	 *
	 * @see Exception(message)
	 */
	Exception(
		const char* const message,
		const Exception& previous) throw();

	/// No-op.
	virtual ~Exception() throw();

	/**
	 * Writes our message to passed stream.
	 *
	 * @exception std::ios_base::failure
	 *            may be thrown by stream, if it is
	 *            configured to do so.
	 */
	void write(std::ostream& out) const throw(std::ios_base::failure);

	/**
	 * Writes a backtrace to passed stream.
	 *
	 * Messages are delimited by one newline and are
	 * printed in reverse chronological order.
	 *
	 * @exception std::ios_base::failure
	 *            may be thrown by stream, if it is
	 *            configured to do so.
	 */
	void backtrace(std::ostream& out) const throw(std::ios_base::failure);

	/**
	 * Creates a shallow copy of this exception.
	 *
	 * Copy is allocated on the heap.
	 *
	 * @throws std::bad_alloc on memory shortage.
	 */
	virtual const Exception* clone() const throw(std::bad_alloc);

	/// Returns a pointer to the internal character string.
	virtual const char* what() const throw();

private:
	/**
	 * Copy of message passed to constructor.
	 *
	 * Length is @ref Exception::LENGTH plus nullbyte.
	 */
	char _message[LENGTH + 1];

	/// Pointer to copy of previous exception, or null.
	const Exception* _previous;

	/// Used by constructors to initialize _message.
	void _initMessage(const char* const message);
};

#endif
