CMAKE_MINIMUM_REQUIRED(VERSION 2.6)

PROJECT(sex)
SET(LIBRARY_NAME           lib${PROJECT_NAME})
SET(PROJECT_BRIEF          "simple exception library (C++)")
SET(PROJECT_VERSION        "3.1.0")
SET(PROJECT_VERSION_DATE   "2011-08-22")


# Compiler Options

ADD_DEFINITIONS("-Werror -Wfatal-errors -Wall -Wextra -ansi -pedantic-errors")
ADD_DEFINITIONS("-Wno-variadic-macros")


# Project Tree

# Only one include directory, for uniformity's sake.
# Hence this one is the only candidate possible.
INCLUDE_DIRECTORIES(.)

# This one contains our code.
ADD_SUBDIRECTORY(${LIBRARY_NAME})

# That one the documentation.
ADD_SUBDIRECTORY(doc EXCLUDE_FROM_ALL)

# That one the programmer tests (not build by default).
ADD_SUBDIRECTORY(tests EXCLUDE_FROM_ALL)
