#ifndef TESTS_UTILITY_HXX
#define TESTS_UTILITY_HXX

#include <cppunit/extensions/HelperMacros.h>

namespace tests
{
	class Utility;
}

class tests::Utility : public CppUnit::TestFixture
{
CPPUNIT_TEST_SUITE(Utility);
	CPPUNIT_TEST(testShortTemplateWithoutFormatting);
	CPPUNIT_TEST(testWhetherFalseIsReturnedWhenHeaderFillsBuffer);
	CPPUNIT_TEST(testWhetherTrueIsReturnedOnExactFit);
	CPPUNIT_TEST(testWhetherUnformattedOverflowsArePrevented);
	CPPUNIT_TEST(testWhetherSimpleFormattingWorks);
	CPPUNIT_TEST(testWhetherFormattedOverflowsArePreventedBeforeFormatstring);
	CPPUNIT_TEST(testWhetherFormattedOverflowsArePreventedInFormatstring);
	CPPUNIT_TEST(testMultipleFormatParameters);
	CPPUNIT_TEST(testWhetherMultipleParametersDontOverflowBeforeFormatstring);
	CPPUNIT_TEST(testWhetherMultipleParametersDontOverflowInFormatstring);
	CPPUNIT_TEST(testTemplateFactory);
	CPPUNIT_TEST(testChainedTemplateFactory);
CPPUNIT_TEST_SUITE_END();

public:
	void testShortTemplateWithoutFormatting();
	void testWhetherFalseIsReturnedWhenHeaderFillsBuffer();
	void testWhetherTrueIsReturnedOnExactFit();
	void testWhetherUnformattedOverflowsArePrevented();
	void testWhetherSimpleFormattingWorks();
	void testWhetherFormattedOverflowsArePreventedBeforeFormatstring();
	void testWhetherFormattedOverflowsArePreventedInFormatstring();
	void testMultipleFormatParameters();
	void testWhetherMultipleParametersDontOverflowBeforeFormatstring();
	void testWhetherMultipleParametersDontOverflowInFormatstring();

	void testTemplateFactory();
	void testChainedTemplateFactory();

	void assertBoundary(
		const char* const buffer,
		size_t boundary,
		size_t length);
};

#endif
