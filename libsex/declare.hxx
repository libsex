/**
 * @file
 *
 * Macro @ref LIBSEX_DECLARE.
 */

#ifndef LIBSEX_DECLARE_HXX
#define LIBSEX_DECLARE_HXX

#include <libsex/Exception.hxx>

/**
 * Declares an exception inheriting from a supplied class.
 *
 * @note Due to technical limitations, it is not possible
 * to use LIBSEX_DECLARE to declare a class in global
 * scope. However, in a truly modular system that would be
 * bad practice anyways, so I don't care.
 *
 * @param parent Parent class (incl. scope).
 * @param scope Non-global scope of class to be defined.
 * @param name Name of class to be defined.
 * @see LIBSEX_DEFINE
 */
#define LIBSEX_DECLARE(parent, scope, name) \
class scope::name : public parent\
{\
public:\
	static const char* const TEMPLATE;\
	name(const char* const errorMessage);\
	name(\
		const char* const errorMessage,\
		const libsex::Exception& previous);\
};

#endif
