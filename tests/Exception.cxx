#include <tests/Exception.hxx>
#include <tests/framework/CountingException.hxx>
#include <tests/framework/UncloneableException.hxx>

#include <cstring>
#include <sstream>

void tests::Exception::testCopyingOfShortMessage()
{
	char* const cstr = new char[80];
	strcpy(cstr, "TEST0001");
	std::string exp(cstr);

	libsex::Exception e(cstr);

	strcpy(cstr, "Invalid.");
	delete cstr;

	std::string act(e.what());
	CPPUNIT_ASSERT_EQUAL(exp, act);
}

void tests::Exception::testWhetherLongMessageDoesNotOverflow()
{
	size_t length = libsex::Exception::LENGTH;
	std::string str;

	for (size_t i = 0; i < length; ++i) {
		str += '.';
	}
	str += '!';

	libsex::Exception e(str.c_str());

	CPPUNIT_ASSERT_EQUAL(length, strlen(e.what()));

	for (size_t i = 0; i < length; ++i) {
		CPPUNIT_ASSERT_EQUAL('.', e.what()[i]);
	}
}

void tests::Exception::testWhetherPreviousExceptionIsCloned()
{
	CPPUNIT_ASSERT_EQUAL(0, CountingException::instances);
	CountingException e1("e1");
	CPPUNIT_ASSERT_EQUAL(1, CountingException::instances);
	libsex::Exception e2("e2", e1);
	CPPUNIT_ASSERT_EQUAL(2, CountingException::instances);
}

void tests::Exception::testWhetherClonedExceptionIsDeleted()
{
	CPPUNIT_ASSERT_EQUAL(0, CountingException::instances);

	CountingException e1("e1");
	CPPUNIT_ASSERT_EQUAL(1, CountingException::instances);
	{
		libsex::Exception e2("e2", e1);
		CPPUNIT_ASSERT_EQUAL(2, CountingException::instances);
	}
	CPPUNIT_ASSERT_EQUAL(1, CountingException::instances);
}

void tests::Exception::testWhetherBadAllocOnCloneIsSwallowed()
{
	UncloneableException e1;
	libsex::Exception e2("e2", e1);
}

void tests::Exception::testWhetherNoticeIsAppendedOnBadAlloc()
{
	UncloneableException e1;
	libsex::Exception e2("e2", e1);

	std::string exp
		= "e2\n"
		  "std::bad_alloc while cloning Exception!";
	std::string act = e2.what();
	CPPUNIT_ASSERT_EQUAL(exp, act);
}

void tests::Exception::testWhetherNoticeDoesNotOverflowIfBufferTooSmall()
{
	size_t length = libsex::Exception::LENGTH;
	std::string str;

	for (size_t i = 0; i < length; ++i) {
		str += '.';
	}

	UncloneableException e1;
	libsex::Exception e2(str.c_str(), e1);

	CPPUNIT_ASSERT_EQUAL(length, strlen(e2.what()));
	CPPUNIT_ASSERT_EQUAL(str, std::string(e2.what()));
}

void tests::Exception::testWritingToStream()
{
	std::string exp("Exception");
	libsex::Exception e(exp.c_str());

	std::stringstream act;
	e.write(act);

	CPPUNIT_ASSERT_EQUAL(exp, act.str());
}

void tests::Exception::testBacktrace()
{
	libsex::Exception cause("Failed to foo.");
	libsex::Exception e("Failed to bar.", cause);

	std::stringstream act;
	e.backtrace(act);

	CPPUNIT_ASSERT_EQUAL(
		std::string("Failed to bar.\nFailed to foo."),
		act.str());
}
