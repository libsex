/**
 * @file
 *
 * Templates of formatting-related internal functions.
 */

#include <libsex/Exception.hxx> // Exception::LENGTH

template <typename T>
T libsex::formatted(
	const char* const file,
	unsigned short line,
	...) throw()
{
	// Temporary string should be initialized
	// with 0 to not forget a trailing 0.
	char buf[Exception::LENGTH + 1] = { 0 };

	va_list ap;
	va_start(ap, line);

	// Fill buf with formatted T::TEMPLATE message.
	vformat(buf, Exception::LENGTH + 1,
		file, line,
		T::TEMPLATE, ap);\
	va_end(ap);

	return T(buf);
}

template <typename T>
T libsex::formatted(
	const Exception& previous,
	const char* const file,
	unsigned short line,
	...) throw()
{
	char buf[Exception::LENGTH + 1] = { 0 };

	va_list ap;
	va_start(ap, line);
	vformat(buf, Exception::LENGTH + 1,
		file, line,
		T::TEMPLATE, ap);\
	va_end(ap);

	return T(buf, previous);
}
