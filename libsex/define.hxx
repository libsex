/**
 * @file
 *
 * Macro @ref LIBSEX_DEFINE.
 */

#ifndef LIBSEX_DEFINE_HXX
#define LIBSEX_DEFINE_HXX

/**
 * Defines an exception inheriting from a supplied parent.
 *
 * @param parent Parent class (incl. scope).
 * @param scope Non-global scope of class to be defined.
 * @param name Name of the class to be defined.
 * @param message Error message template.
 * @see LIBSEX_DECLARE
 * @see snprintf
 */
#define LIBSEX_DEFINE(parent, scope, name, message) \
const char* const scope::name::TEMPLATE = message;\
\
scope::name::name(const char* const errorMessage)\
: parent(errorMessage)\
{\
}\
\
scope::name::name(\
	const char* const errorMessage,\
	const libsex::Exception& previous)\
: parent(errorMessage, previous)\
{\
}\

#endif
