#ifndef MOCKEXCEPTION_HXX
#define MOCKEXCEPTION_HXX

#include <libsex/Exception.hxx>

class MockException : public libsex::Exception
{
public:
	static const char* const TEMPLATE;

	MockException(const char* const message) throw();
	MockException(
		const char* const message,
		const libsex::Exception& previous) throw();
};

#endif
