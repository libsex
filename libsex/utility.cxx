/**
 * @file
 *
 * Non-template definitions of formatting-related internal
 * functions.
 */

#include <libsex/utility.hxx>
#include <cassert>

bool libsex::vformat(
	char* const buf,
	size_t len,
	const char* const file,
	unsigned short line,
	const char* const msg,
	va_list ap) throw()
{
	size_t chars = snprintf(buf, len, "%s:%d: ", file, line);

	// If buffer was too small, chars contains length
	// of chars that would've been written. Chars must
	// be lower than length or else we could not fit
	// the message into buffer, too.
	bool didFitIntoBuffer = chars < len;

	if (didFitIntoBuffer) {
		size_t      newLength = len - chars;
		char* const newBuffer = buf + chars;
		chars = vsnprintf(newBuffer, newLength, msg, ap);
		didFitIntoBuffer = chars <= newLength;
	}

	return didFitIntoBuffer;
}

bool libsex::format(
	char* const buf,
	size_t len,
	const char* const file,
	unsigned short line,
	const char* const msg,
	...) throw()
{
	va_list ap;
	va_start(ap, msg);

	bool result = vformat(buf, len, file, line, msg, ap);

	va_end(ap);
	return result;
}
