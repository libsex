#include <cppunit/extensions/HelperMacros.h>

// Registering all unit tests here, to make them
// easier to disable and enable.

#include <tests/Exception.hxx>
CPPUNIT_TEST_SUITE_REGISTRATION(tests::Exception);

#include <tests/Utility.hxx>
CPPUNIT_TEST_SUITE_REGISTRATION(tests::Utility);

#include <tests/UsageExamples.hxx>
CPPUNIT_TEST_SUITE_REGISTRATION(tests::UsageExamples);
