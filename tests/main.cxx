#include <iostream>

#include <cppunit/TestResult.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/BriefTestProgressListener.h>
#include <cppunit/TextOutputter.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/TestRunner.h>

int main()
{
	CppUnit::TestResult test;
	CppUnit::TestResultCollector result;
	CppUnit::BriefTestProgressListener progress;
	test.addListener(&result);
	test.addListener(&progress);

	CppUnit::TextOutputter outputter(&result, std::clog);

	CppUnit::TestRunner runner;
	runner.addTest(CppUnit::TestFactoryRegistry::getRegistry().makeTest());
	runner.run(test);

	outputter.write();

	return result.wasSuccessful() ? 0 : 1;
}
