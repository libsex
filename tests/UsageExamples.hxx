#ifndef TESTS_USAGEEXAMPLES_HXX
#define TESTS_USAGEEXAMPLES_HXX

#include <cppunit/extensions/HelperMacros.h>

namespace tests
{
	class UsageExamples;
}

class tests::UsageExamples : public CppUnit::TestFixture
{
CPPUNIT_TEST_SUITE(UsageExamples);
	CPPUNIT_TEST(testSimpleConstruction);
	CPPUNIT_TEST(testSimpleThrowing);
	CPPUNIT_TEST(testThrowing);
	CPPUNIT_TEST(testChaining);
CPPUNIT_TEST_SUITE_END();

public:
	void testSimpleConstruction();
	void testSimpleThrowing();
	void testThrowing();
	void testChaining();
};

#endif
