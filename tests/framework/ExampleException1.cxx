/**
 * @file
 * Shows how to _define_ a basic exception class.
 */

#include <libsex/define.hxx>
#include <libsex/Exception.hxx>
#include <tests/framework/ExampleException1.hxx>

// Define ExampleException1 and provide a message template.
// If you want to localize your exceptions, you just have
// to localize that line.

LIBSEX_DEFINE(libsex::Exception, example, ExampleException1, "Error!")
