/**
 * @file
 *
 * Shows how to _declare_ basic exception class.
 */

#ifndef EXAMPLEEXCEPTION1_HXX
#define EXAMPLEEXCEPTION1_HXX

#include <libsex/declare.hxx>
#include <libsex/Exception.hxx>


namespace example
{
	// Forward declare to ensure the namespace exists.
	class ExampleException1;
}


// ExampleException1 inherits from libsex::Exception.
LIBSEX_DECLARE(libsex::Exception, example, ExampleException1)

#endif
