/**
 * @file
 *
 * Shows how to _define_ an exception class that contains
 * placeholders in its message template.
 */

#include <libsex/define.hxx>
#include <libsex/Exception.hxx>
#include <tests/framework/ExampleException2.hxx>

// Define ExampleException2 and provide a message template
// (see printf for a list and explanation of placeholders).

LIBSEX_DEFINE(libsex::Exception, example, ExampleException2, "%s is %d")
