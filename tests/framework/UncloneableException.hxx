#ifndef UNCLONEABLEEXCEPTION_HXX
#define UNCLONEABLEEXCEPTION_HXX

#include <libsex/Exception.hxx>

class UncloneableException : public libsex::Exception
{
public:
	UncloneableException();
	~UncloneableException() throw();

	const libsex::Exception*
	clone() const throw(std::bad_alloc);
};

#endif
