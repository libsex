#ifndef COUNTINGEXCEPTION_HXX
#define COUNTINGEXCEPTION_HXX

#include <libsex/Exception.hxx>

class CountingException : public libsex::Exception
{
public:
	static int instances;

	CountingException(const char* const);
	~CountingException() throw();

	const libsex::Exception*
	clone() const throw(std::bad_alloc);
};

#endif
